module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'import/prefer-default-export': 'off',
    'linebreak-style': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
  },
};
