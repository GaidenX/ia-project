/* eslint-disable no-param-reassign */

module.exports = {
  setHelpLinks(context, params) {
    context.state.messageHelpLink.push(params);
  },
  setLoggerMessages(context, params) {
    context.state.loggerMessages = params;
  },
};
