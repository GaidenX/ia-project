module.exports = {
  getHelpLinks(context) {
    return context.state.messageHelpLink;
  },
  getLoggerMessages(context) {
    return context.state.loggerMessages;
  },
};
