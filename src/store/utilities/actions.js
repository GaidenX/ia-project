module.exports = {
  commitHelpLinks(context, params) {
    context.commit('setHelpLinks', params);
  },
  commitLoggerMessages(context, params) {
    context.commit('setLoggerMessages', params);
  },
};
