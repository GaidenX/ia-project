const state = require('./state');
const getters = require('./getters');
const mutations = require('./mutations');
const actions = require('./actions');

module.exports = {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
};
