import Vue from 'vue';
import Vuex from 'vuex';

const utilities = require('./utilities/module');

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    utilities,
  },
});
