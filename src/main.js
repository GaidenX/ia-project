import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';

Vue.config.productionTip = false;

require('./assets/src/app.scss');
require('./assets/src/bulma_helper.scss');

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
