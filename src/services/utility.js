import { http, httpApiHelp } from './config';

export default {
  getLogs() {
    return http.get('getlog');
  },
  getHelpLink(params) {
    // eslint-disable-next-line no-param-reassign
    // params = encodeURIComponent(params.trim());
    console.log(params);
    return httpApiHelp.get(
      `?order=desc&sort=activity&tagged=javascript&intitle=${params}&filter=default&site=stackoverflow`,
    );
  },
};
