import axios from 'axios';

export const http = axios.create({
  baseURL: 'https://amenoiabackend.us-south.cf.appdomain.cloud/',
});

export const httpApiHelp = axios.create({
  baseURL: 'https://api.stackexchange.com/2.2/search/',
});
